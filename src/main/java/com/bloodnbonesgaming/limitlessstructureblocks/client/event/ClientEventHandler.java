package com.bloodnbonesgaming.limitlessstructureblocks.client.event;

import com.bloodnbonesgaming.limitlessstructureblocks.gui.GuiEditStructureOverride;

import net.minecraft.client.gui.inventory.GuiEditStructure;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientEventHandler {

	@SubscribeEvent
	public void onGui(final GuiOpenEvent event)
	{
		if (event.getGui() instanceof GuiEditStructure)
		{
			event.setGui(new GuiEditStructureOverride(((GuiEditStructure) event.getGui()).tileStructure));
		}
	}
}
