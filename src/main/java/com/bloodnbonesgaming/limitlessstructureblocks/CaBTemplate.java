package com.bloodnbonesgaming.limitlessstructureblocks;

import java.util.List;

import com.google.common.collect.Lists;

import mod.chiselsandbits.chiseledblock.NBTBlobConverter;
import mod.chiselsandbits.chiseledblock.TileEntityBlockChiseled;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.template.Template;

public class CaBTemplate extends Template {
	
	
	@Override
	public void takeBlocksFromWorld(World worldIn, BlockPos startPos, BlockPos endPos, boolean takeEntities,
			Block toIgnore) {
		if (endPos.getX() >= 1 && endPos.getY() >= 1 && endPos.getZ() >= 1)
        {
            BlockPos blockpos = startPos.add(endPos).add(-1, -1, -1);
            List<Template.BlockInfo> list = Lists.<Template.BlockInfo>newArrayList();
            List<Template.BlockInfo> list1 = Lists.<Template.BlockInfo>newArrayList();
            List<Template.BlockInfo> list2 = Lists.<Template.BlockInfo>newArrayList();
            BlockPos blockpos1 = new BlockPos(Math.min(startPos.getX(), blockpos.getX()), Math.min(startPos.getY(), blockpos.getY()), Math.min(startPos.getZ(), blockpos.getZ()));
            BlockPos blockpos2 = new BlockPos(Math.max(startPos.getX(), blockpos.getX()), Math.max(startPos.getY(), blockpos.getY()), Math.max(startPos.getZ(), blockpos.getZ()));
            this.size = endPos;

            for (BlockPos.MutableBlockPos blockpos$mutableblockpos : BlockPos.getAllInBoxMutable(blockpos1, blockpos2))
            {
                BlockPos blockpos3 = blockpos$mutableblockpos.subtract(blockpos1);
                IBlockState iblockstate = worldIn.getBlockState(blockpos$mutableblockpos);

                if (toIgnore == null || toIgnore != iblockstate.getBlock())
                {
                    TileEntity tileentity = worldIn.getTileEntity(blockpos$mutableblockpos);

                    if (tileentity != null)
                    {
                        NBTTagCompound nbttagcompound = tileentity.writeToNBT(new NBTTagCompound());
                        nbttagcompound.removeTag("x");
                        nbttagcompound.removeTag("y");
                        nbttagcompound.removeTag("z");
                        
                    	if (tileentity instanceof TileEntityBlockChiseled)
                    	{
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_SIDE_FLAGS))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_SIDE_FLAGS);
                    		}
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_NORMALCUBE_FLAG))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_NORMALCUBE_FLAG);
                    		}
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_LIGHTVALUE))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_LIGHTVALUE);
                    		}
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_PRIMARY_STATE))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_PRIMARY_STATE);
                    		}
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_LEGACY_VOXEL))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_LEGACY_VOXEL);
                    		}
                    		if (nbttagcompound.hasKey(NBTBlobConverter.NBT_VERSIONED_VOXEL))
                    		{
                    			nbttagcompound.removeTag(NBTBlobConverter.NBT_VERSIONED_VOXEL);
                    		}
                    		TileEntityBlockChiseled chiseled = (TileEntityBlockChiseled) tileentity;
                    		new NBTBlobConverter( false, chiseled ).writeChisleData( nbttagcompound, true );
                    	}
                        list1.add(new Template.BlockInfo(blockpos3, iblockstate, nbttagcompound));
                    }
                    else if (!iblockstate.isFullBlock() && !iblockstate.isFullCube())
                    {
                        list2.add(new Template.BlockInfo(blockpos3, iblockstate, (NBTTagCompound)null));
                    }
                    else
                    {
                        list.add(new Template.BlockInfo(blockpos3, iblockstate, (NBTTagCompound)null));
                    }
                }
            }

            this.blocks.clear();
            this.blocks.addAll(list);
            this.blocks.addAll(list1);
            this.blocks.addAll(list2);

            if (takeEntities)
            {
                this.takeEntitiesFromWorld(worldIn, blockpos1, blockpos2.add(1, 1, 1));
            }
            else
            {
                this.entities.clear();
            }
        }
	}
}
