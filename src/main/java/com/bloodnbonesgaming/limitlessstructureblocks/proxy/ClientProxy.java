package com.bloodnbonesgaming.limitlessstructureblocks.proxy;

import com.bloodnbonesgaming.limitlessstructureblocks.client.event.ClientEventHandler;

import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerEventHandlers() {
		super.registerEventHandlers();
		MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
	}
}
