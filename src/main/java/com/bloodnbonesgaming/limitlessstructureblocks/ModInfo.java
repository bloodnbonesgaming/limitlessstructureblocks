package com.bloodnbonesgaming.limitlessstructureblocks;

public class ModInfo {
	public static final String MODID = "limitlessstructureblocks";
	public static final String MOD_NAME = "Limitless Structure Blocks";
	public static final String VERSION = "@VERSION@";
	public static final String SERVER_PROXY = "com.bloodnbonesgaming.limitlessstructureblocks.proxy.ServerProxy";
	public static final String CLIENT_PROXY = "com.bloodnbonesgaming.limitlessstructureblocks.proxy.ClientProxy";
	
	public static final String SERVER_MESSAGE_CONSUMER = "com.bloodnbonesgaming.limitlessstructureblocks.network.ServerMessageConsumer";
}