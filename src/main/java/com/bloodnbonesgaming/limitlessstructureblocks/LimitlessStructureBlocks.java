package com.bloodnbonesgaming.limitlessstructureblocks;

import org.apache.logging.log4j.Logger;

import com.bloodnbonesgaming.limitlessstructureblocks.blocks.StructureBlockOverride;
import com.bloodnbonesgaming.limitlessstructureblocks.blocks.TileEntityStructureOverride;
import com.bloodnbonesgaming.limitlessstructureblocks.network.PacketSyncStructureBlock;
import com.bloodnbonesgaming.limitlessstructureblocks.network.ServerMessageConsumer;
import com.bloodnbonesgaming.limitlessstructureblocks.proxy.CommonProxy;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModInfo.MODID, name = ModInfo.MOD_NAME, version = ModInfo.VERSION, dependencies = "after:chiselsandbits;", acceptedMinecraftVersions = "[1.12,1.13)")
public class LimitlessStructureBlocks {
	
	@Instance(ModInfo.MODID)
	public static LimitlessStructureBlocks instance;
	
	@SidedProxy(clientSide = ModInfo.CLIENT_PROXY, serverSide = ModInfo.SERVER_PROXY)
    public static CommonProxy proxy;
	
	public static final SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel(ModInfo.MODID);
	
	private Logger log;
	
	public static boolean chiselAndBits = false;
	
	@EventHandler
	public void preInit(final FMLPreInitializationEvent event)
	{
		this.log = event.getModLog();
	    proxy.registerEventHandlers();
	    MinecraftForge.EVENT_BUS.register(LimitlessStructureBlocks.instance);
	    
        if (Loader.isModLoaded("chiselsandbits"))
        {
        	LimitlessStructureBlocks.chiselAndBits = true;
        }
	}
	
	@EventHandler
	public void init(final FMLInitializationEvent event)
	{
		network.registerMessage(ServerMessageConsumer.class, PacketSyncStructureBlock.class, 0, Side.SERVER);
	}
	
	@EventHandler
	public void postInit(final FMLPostInitializationEvent event)
	{
	}
	
	@EventHandler
	public void serverAboutToSTart(final FMLServerAboutToStartEvent event)
	{
	}
	
	@EventHandler
	public void serverStarting(final FMLServerStartingEvent event)
	{
	}
	
	@EventHandler
	public void serverStarted(final FMLServerStartedEvent event)
	{
	}
	
	@EventHandler
    public void serverStopped(final FMLServerStoppedEvent event)
    {
    }
	
	public static StructureBlockOverride structure;
	
	@SubscribeEvent
	public void onRegisterBlocks(RegistryEvent.Register<Block> event)
	{
		structure = new StructureBlockOverride();
		GameRegistry.registerTileEntity(TileEntityStructureOverride.class, new ResourceLocation(ModInfo.MODID, "tile_entity_structure_override"));
		event.getRegistry().register(structure);
		
		LimitlessStructureBlocks.instance.getLog().info("Registered structure block");
	}

	public Logger getLog() {
		return this.log;
	}
}