package com.bloodnbonesgaming.limitlessstructureblocks.blocks;

import java.util.Random;

import com.bloodnbonesgaming.limitlessstructureblocks.ModInfo;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockStructure;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class StructureBlockOverride extends BlockStructure
{
	public StructureBlockOverride()
	{
        this.setUnlocalizedName("structureBlock");
		this.setRegistryName("minecraft", "structure_block");
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityStructureOverride();
	}
	
//    public static final PropertyEnum<TileEntityStructureOverride.Mode> MODE = PropertyEnum.<TileEntityStructureOverride.Mode>create("mode", TileEntityStructureOverride.Mode.class);
//
//    public StructureBlockOverride()
//    {
//        super(Material.IRON, MapColor.SILVER);
//        this.setDefaultState(this.blockState.getBaseState());
//    }
//
//    /**
//     * Returns a new instance of a block's tile entity class. Called on placing the block.
//     */
//    public TileEntity createNewTileEntity(World worldIn, int meta)
//    {
//        return new TileEntityStructureOverride();
//    }
//
//    /**
//     * Called when the block is right clicked by a player.
//     */
//    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
//    {
//        TileEntity tileentity = worldIn.getTileEntity(pos);
//        return tileentity instanceof TileEntityStructureOverride ? ((TileEntityStructureOverride)tileentity).usedBy(playerIn) : false;
//    }
//
//    /**
//     * Called by ItemBlocks after a block is set in the world, to allow post-place logic
//     */
//    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
//    {
//        if (!worldIn.isRemote)
//        {
//            TileEntity tileentity = worldIn.getTileEntity(pos);
//
//            if (tileentity instanceof TileEntityStructureOverride)
//            {
//            	TileEntityStructureOverride tileentitystructure = (TileEntityStructureOverride)tileentity;
//                tileentitystructure.createdBy(placer);
//            }
//        }
//    }
//
//    /**
//     * Returns the quantity of items to drop on block destruction.
//     */
//    public int quantityDropped(Random random)
//    {
//        return 0;
//    }
//
//    /**
//     * The type of render function called. MODEL for mixed tesr and static model, MODELBLOCK_ANIMATED for TESR-only,
//     * LIQUID for vanilla liquids, INVISIBLE to skip all rendering
//     */
//    public EnumBlockRenderType getRenderType(IBlockState state)
//    {
//        return EnumBlockRenderType.MODEL;
//    }
//
//    /**
//     * Called by ItemBlocks just before a block is actually set in the world, to allow for adjustments to the
//     * IBlockstate
//     */
//    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
//    {
//        return this.getDefaultState().withProperty(MODE, TileEntityStructureOverride.Mode.DATA);
//    }
//
//    /**
//     * Convert the given metadata into a BlockState for this Block
//     */
//    public IBlockState getStateFromMeta(int meta)
//    {
//        return this.getDefaultState().withProperty(MODE, TileEntityStructureOverride.Mode.getById(meta));
//    }
//
//    /**
//     * Convert the BlockState into the correct metadata value
//     */
//    public int getMetaFromState(IBlockState state)
//    {
//        return ((TileEntityStructureOverride.Mode)state.getValue(MODE)).getModeId();
//    }
//
//    protected BlockStateContainer createBlockState()
//    {
//        return new BlockStateContainer(this, new IProperty[] {MODE});
//    }
//
//    /**
//     * Called when a neighboring block was changed and marks that this state should perform any checks during a neighbor
//     * change. Cases may include when redstone power is updated, cactus blocks popping off due to a neighboring solid
//     * block, etc.
//     */
//    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
//    {
//        if (!worldIn.isRemote)
//        {
//            TileEntity tileentity = worldIn.getTileEntity(pos);
//
//            if (tileentity instanceof TileEntityStructureOverride)
//            {
//            	TileEntityStructureOverride tileentitystructure = (TileEntityStructureOverride)tileentity;
//                boolean flag = worldIn.isBlockPowered(pos);
//                boolean flag1 = tileentitystructure.isPowered();
//
//                if (flag && !flag1)
//                {
//                    tileentitystructure.setPowered(true);
//                    this.trigger(tileentitystructure);
//                }
//                else if (!flag && flag1)
//                {
//                    tileentitystructure.setPowered(false);
//                }
//            }
//        }
//    }
//
//    private void trigger(TileEntityStructureOverride p_189874_1_)
//    {
//        switch (p_189874_1_.getMode())
//        {
//            case SAVE:
//                p_189874_1_.save(false);
//                break;
//            case LOAD:
//                p_189874_1_.load(false);
//                break;
//            case CORNER:
//                p_189874_1_.unloadStructure();
//            case DATA:
//        }
//    }
}