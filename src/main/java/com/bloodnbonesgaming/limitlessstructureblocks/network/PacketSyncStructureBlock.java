package com.bloodnbonesgaming.limitlessstructureblocks.network;

import java.nio.charset.StandardCharsets;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class PacketSyncStructureBlock implements IMessage
{
    public BlockPos pos;
    public byte type;
    public String mode;
    public String name;
    public int posX;
    public int posY;
    public int posZ;
    public int sizeX;
    public int sizeY;
    public int sizeZ;
    public String mirror;
    public String rotation;
    public String data;
    public boolean ignoresEntities;
    public boolean showsAir;
    public boolean showsBoundingBox;
    public float integrity;
    public long seed;

    @Override
    public void fromBytes(final ByteBuf buf) {
    	this.pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
    	this.type = buf.readByte();
    	this.mode = buf.readCharSequence(buf.readInt(), StandardCharsets.UTF_8).toString();
    	this.name = buf.readCharSequence(buf.readInt(), StandardCharsets.UTF_8).toString();
    	this.posX = buf.readInt();
    	this.posY = buf.readInt();
    	this.posZ = buf.readInt();
    	this.sizeX = buf.readInt();
    	this.sizeY = buf.readInt();
    	this.sizeZ = buf.readInt();
    	this.mirror = buf.readCharSequence(buf.readInt(), StandardCharsets.UTF_8).toString();
    	this.rotation = buf.readCharSequence(buf.readInt(), StandardCharsets.UTF_8).toString();
    	this.data = buf.readCharSequence(buf.readInt(), StandardCharsets.UTF_8).toString();
    	this.ignoresEntities = buf.readBoolean();
    	this.showsAir = buf.readBoolean();
    	this.showsBoundingBox = buf.readBoolean();
    	this.integrity = buf.readFloat();
    	this.seed = buf.readLong();
    }

    @Override
    public void toBytes(final ByteBuf buf) {
    	buf.writeInt(this.pos.getX());
    	buf.writeInt(this.pos.getY());
    	buf.writeInt(this.pos.getZ());
    	buf.writeByte(this.type);
    	buf.writeInt(this.mode.length());
    	buf.writeCharSequence(this.mode, StandardCharsets.UTF_8);
    	buf.writeInt(this.name.length());
    	buf.writeCharSequence(this.name, StandardCharsets.UTF_8);
    	buf.writeInt(this.posX);
    	buf.writeInt(this.posY);
    	buf.writeInt(this.posZ);
    	buf.writeInt(this.sizeX);
    	buf.writeInt(this.sizeY);
    	buf.writeInt(this.sizeZ);
    	buf.writeInt(this.mirror.length());
    	buf.writeCharSequence(this.mirror, StandardCharsets.UTF_8);
    	buf.writeInt(this.rotation.length());
    	buf.writeCharSequence(this.rotation, StandardCharsets.UTF_8);
    	buf.writeInt(this.data.length());
    	buf.writeCharSequence(this.data, StandardCharsets.UTF_8);
    	buf.writeBoolean(this.ignoresEntities);
    	buf.writeBoolean(this.showsAir);
    	buf.writeBoolean(this.showsBoundingBox);
    	buf.writeFloat(this.integrity);
    	buf.writeLong(this.seed);
    }
}
