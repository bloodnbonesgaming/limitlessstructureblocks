package com.bloodnbonesgaming.limitlessstructureblocks.network;

import com.bloodnbonesgaming.limitlessstructureblocks.LimitlessStructureBlocks;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityStructure;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerMessageConsumer implements IMessageHandler<PacketSyncStructureBlock, IMessage> {

    @Override
	public IMessage onMessage(PacketSyncStructureBlock message, MessageContext ctx) {
    	final EntityPlayer player = ctx.getServerHandler().player;
    	
        player.getServer().addScheduledTask(() -> {
        	LimitlessStructureBlocks.instance.getLog().info("Received Structure Block sync message from client");
        	
        	if (!player.canUseCommandBlock())
            {
                return;
            }

            try
            {
                BlockPos blockpos = message.pos;
                if (player.world.isBlockLoaded(blockpos))
                {
                	IBlockState iblockstate1 = player.world.getBlockState(blockpos);
                    TileEntity tileentity1 =player.world.getTileEntity(blockpos);

                    if (tileentity1 instanceof TileEntityStructure)
                    {
                        TileEntityStructure tileentitystructure = (TileEntityStructure)tileentity1;
                        int l1 = message.type;
                        String s8 = message.mode;
                        tileentitystructure.setMode(TileEntityStructure.Mode.valueOf(s8));
                        tileentitystructure.setName(message.name);
                        int i2 = message.posX;
                        int j2 = message.posY;
                        int k2 = message.posZ;
                        tileentitystructure.setPosition(new BlockPos(i2, j2, k2));
                        int l2 = message.sizeX;
                        int i3 = message.sizeY;
                        int j = message.sizeZ;
                        tileentitystructure.setSize(new BlockPos(l2, i3, j));
                        String s2 = message.mirror;
                        tileentitystructure.setMirror(Mirror.valueOf(s2));
                        String s3 = message.rotation;
                        tileentitystructure.setRotation(Rotation.valueOf(s3));
                        tileentitystructure.setMetadata(message.data);
                        tileentitystructure.setIgnoresEntities(message.ignoresEntities);
                        tileentitystructure.setShowAir(message.showsAir);
                        tileentitystructure.setShowBoundingBox(message.showsBoundingBox);
                        tileentitystructure.setIntegrity(MathHelper.clamp(message.integrity, 0.0F, 1.0F));
                        tileentitystructure.setSeed(message.seed);
                        String s4 = tileentitystructure.getName();

                        if (l1 == 2)
                        {
                            if (tileentitystructure.save())
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.save_success", new Object[] {s4}), false);
                            }
                            else
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.save_failure", new Object[] {s4}), false);
                            }
                        }
                        else if (l1 == 3)
                        {
                            if (!tileentitystructure.isStructureLoadable())
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.load_not_found", new Object[] {s4}), false);
                            }
                            else if (tileentitystructure.load())
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.load_success", new Object[] {s4}), false);
                            }
                            else
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.load_prepare", new Object[] {s4}), false);
                            }
                        }
                        else if (l1 == 4)
                        {
                            if (tileentitystructure.detectSize())
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.size_success", new Object[] {s4}), false);
                            }
                            else
                            {
                                player.sendStatusMessage(new TextComponentTranslation("structure_block.size_failure", new Object[0]), false);
                            }
                        }

                        tileentitystructure.markDirty();
                       player.world.notifyBlockUpdate(blockpos, iblockstate1, iblockstate1, 3);
                    }
                }
            }
            catch (Exception e)
            {
            	e.printStackTrace();
            }
        });
        return null;
	}

}
